export const CONFIG_IDENTIFIER = {
  IDatabaseConnectionProviderConfig: Symbol.for('IDatabaseConnectionProviderConfig'),
  IRabbitMqConnectionProviderConfig: Symbol.for('IRabbitMqConnectionProviderConfig'),
};
