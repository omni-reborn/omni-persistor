export const REPOSITORY_IDENTIFIER = {
  TypeOrmMessageRepository: Symbol.for('TypeOrmMessageRepository'),
  IMessageRepository: Symbol.for('IMessageRepository'),
};
