import * as path from 'path';
import { loadConfig } from 'src/config';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { ContainerModule, interfaces } from 'inversify';

export const configModule = new ContainerModule(
  (bind: interfaces.Bind) => {
  },
);
