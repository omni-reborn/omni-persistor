import { SERVICE_IDENTIFIER, REPOSITORY_IDENTIFIER } from 'src/constants';
import { IDatabaseConnectionProvider, DatabaseConnectionProvider } from 'src/database';
import { AsyncContainerModule, interfaces } from 'inversify';
import { Repository as TypeOrmRepository } from 'typeorm';
import { loadConfig } from 'src/config';
import { IDatabaseConnectionProviderConfig, TDatabaseConnectionProviderConfig } from 'src/config/database';
import { MessageEntity } from 'src/entity';
import { IMessageRepository, MessageRepository } from 'src/repository/message';

export const databaseModule = new AsyncContainerModule(
  async (bind: interfaces.Bind) => {
    const providerConfig = loadConfig<IDatabaseConnectionProviderConfig>(TDatabaseConnectionProviderConfig, './config/connectionProviderConfig.yml');
    const connectionProvider = new DatabaseConnectionProvider(providerConfig);
    const connection = await connectionProvider.getConnection();

    bind<IDatabaseConnectionProvider>(SERVICE_IDENTIFIER.IDatabaseConnectionProvider)
      .toConstantValue(connectionProvider);

    bind<TypeOrmRepository<MessageEntity>>(REPOSITORY_IDENTIFIER.TypeOrmMessageRepository)
      .toConstantValue(connection.getRepository(MessageEntity));

    bind<IMessageRepository>(REPOSITORY_IDENTIFIER.IMessageRepository)
      .to(MessageRepository)
      .inRequestScope();
  },
);
