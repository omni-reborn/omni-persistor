import { Request, Response, NextFunction } from 'express';
import { interfaces, controller, httpGet, response } from 'inversify-express-utils';
import { inject } from 'inversify';
import { REPOSITORY_IDENTIFIER } from 'src/constants';
import { IMessageRepository } from 'src/repository/message';
import { DatabaseError } from 'src/error';

@controller('/message')
export class MessageController implements interfaces.Controller {
  private messageRepository: IMessageRepository;

  public constructor(@inject(REPOSITORY_IDENTIFIER.IMessageRepository) messageRepository: IMessageRepository) {
    this.messageRepository = messageRepository;
  }

  @httpGet('/')
  private async getAllMessages(@response() res: Response) {
    try {
      const result = await this.messageRepository.getAll();
      res.status(200).json({ data: result });
    } catch (error) {
      if (error instanceof DatabaseError) {
        res.status(500).json({ error: 'Internal server error.' });
      }
    }
  }
}
