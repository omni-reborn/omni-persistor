import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import { installContainer } from 'src/container/installer';
import { InversifyExpressServer } from 'inversify-express-utils';

async function main() {
  try {
    const container = await installContainer();
    const server = new InversifyExpressServer(container);

    server.setConfig((app) => {
      app.use(logger('dev'));
      app.use(bodyParser.json());
      app.use(bodyParser.urlencoded({ extended: true }));
    });

    const expressApp = server.build();
    expressApp.listen(3000);
  } catch (error) {
    console.log(error);
  }
}

main();
