import { GenericRepository, IRepository } from 'src/repository/base';
import { MessageDTO, MessageEntity } from 'src/entity';
import { injectable, inject } from 'inversify';
import { Repository as TypeOrmRepository } from 'typeorm';
import { REPOSITORY_IDENTIFIER } from 'src/constants/repositoryIdentifier';
import { MessageDataMapper } from './dataMapper';
import { DatabaseError } from 'src/error';

export interface IMessageRepository extends IRepository<MessageDTO> {
  getAllByAuthor(author: string): Promise<Array<MessageDTO>>;
  getAllByChannel(channel: string): Promise<Array<MessageDTO>>;
  getAllByGuild(guild: string): Promise<Array<MessageDTO>>;
}

@injectable()
export class MessageRepository
  extends GenericRepository<MessageDTO, MessageEntity>
  implements IMessageRepository {

  public constructor(
    @inject(REPOSITORY_IDENTIFIER.TypeOrmMessageRepository) repository: TypeOrmRepository<MessageEntity>,
  ) {
    super(repository, new MessageDataMapper());
  }

  /**
   * Gets all messages by a given author.
   * @param author author ID
   */
  public async getAllByAuthor(author: string) {
    try {
      const entities = await this._repository.find({ author });
      return entities.map((e) => this._dataMapper.toDomain(e));
    } catch (error) {
      throw new DatabaseError('Error while getting entities from the database.');
    }
  }

  /**
   * Gets all messages from a given channel.
   * @param channel channel ID
   */
  public async getAllByChannel(channel: string) {
    try {
      const entities = await this._repository.find({ channel });
      return entities.map((e) => this._dataMapper.toDomain(e));
    } catch (error) {
      throw new DatabaseError('Error while getting entities from the database.');
    }
  }

  /**
   * Gets all messages from a given guild.
   * @param guild guild ID
   */
  public async getAllByGuild(guild: string) {
    try {
      const entities = await this._repository.find({ guild });
      return entities.map((e) => this._dataMapper.toDomain(e));
    } catch (error) {
      throw new DatabaseError('Error while getting entities from the database.');
    }
  }
}
