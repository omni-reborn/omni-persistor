/* tslint:disable: no-any */
export function reduceBoolean(array: Array<boolean>): boolean {
  return array.reduce((prev, next) => prev && next);
}
