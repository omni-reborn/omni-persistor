import { IDatabaseConnectionProviderConfig } from 'src/config/database';

export const mockConfig: IDatabaseConnectionProviderConfig = {
  type: 'postgres',
  host: 'test',
  port: '1',
  username: 'test',
  password: 'test',
  database: 'test',
};
