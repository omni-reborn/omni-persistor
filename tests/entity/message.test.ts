import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import 'mocha';
import { MessageEntity, MessageDTO } from 'src/entity';
import { reduceBoolean } from 'src/util';

chai.use(sinonChai);
const expect = chai.expect;

describe('Message Entity', () => {
  it('Should create an empty entity', () => {
    const entity = new MessageEntity();
    const assertionArray = Object.keys(entity).map((key) => entity[key] === undefined);
    expect(reduceBoolean(assertionArray)).to.be.true;
  });

  it('Should accept values from DTO on creation', () => {
    const dto: MessageDTO = {
      id: '0',
      messageId: '0',
      body: 'body',
      author: 'user',
      channel: '0',
      guild: 'guild',
      timestamp: new Date(),
    };

    const entity = new MessageEntity(dto);
    const assertionArray = Object.keys(entity).map((key) => entity[key] === dto[key]);
    expect(reduceBoolean(assertionArray)).to.be.true;
  });

  it('Should accept values without guild property', () => {
    const dto: MessageDTO = {
      id: '0',
      messageId: '0',
      body: 'body',
      author: 'user',
      channel: '0',
      timestamp: new Date(),
    };

    const entity = new MessageEntity(dto);
    const assertionArray = Object.keys(entity).map((key) => entity[key] === dto[key]);
    expect(reduceBoolean(assertionArray)).to.be.true;
  });
});
