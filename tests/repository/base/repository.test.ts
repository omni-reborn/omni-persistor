import * as chai from 'chai';
import * as sinon from 'sinon';
import * as sinonChai from 'sinon-chai';
import 'mocha';

chai.use(sinonChai);
const expect = chai.expect;

import { Repository } from 'typeorm';
import { MockDTO, MockEntity } from './mockEntity';
import { MockDataMapper } from './mockDataMapper';
import { GenericRepository } from 'src/repository/base';
import { MockRepository } from '../typeorm/mockRepository';
import { DatabaseError } from 'src/error';

describe('Generic Repository', () => {
  const sandbox = sinon.createSandbox();
  const dataMapper = new MockDataMapper();

  let genericRepository: GenericRepository<MockDTO, MockEntity>;
  let typeOrmRepository: MockRepository<MockEntity>;

  describe('Success', () => {
    before(() => {
      typeOrmRepository = {
        find: sandbox.stub(),
        insert: sandbox.stub(),
        remove: sandbox.stub(),
      };

      typeOrmRepository.find.withArgs().resolves([new MockEntity({ id: '1', body: 'test1' }), new MockEntity({ id: '2', body: 'test2' })]);
      typeOrmRepository.remove.withArgs([new MockEntity({ id: '1', body: 'test1' })]).resolves();
      typeOrmRepository.insert.withArgs([new MockEntity({ id: '1', body: 'test1' })]).resolves();

      // To circumvent sinon overload issue, we create our custom repo mock and cast through unknown to insert
      genericRepository = new GenericRepository<MockDTO, MockEntity>(typeOrmRepository as unknown as Repository<MockEntity>, dataMapper);
    });

    after(() => {
      sandbox.restore();
    });

    it('getAll() method should return all rows from an entity', async () => {
      const result = await genericRepository.getAll();
      expect(typeOrmRepository.find.calledOnce).to.be.true;
      expect(result).to.be.an('array');
      expect(result.length).to.be.equal(2);
      expect(result[0]).to.deep.equal({ id: '1', body: 'test1' });
      expect(result[1]).to.deep.equal({ id: '2', body: 'test2' });
    });

    it('remove() method should remove items from the db', async () => {
      await genericRepository.remove([{ id: '1', body: 'test1' }]);
      expect(typeOrmRepository.remove.calledOnce).to.be.true;
    });

    it('insert() method should insert an an item to the db', async () => {
      await genericRepository.insert([{ id: '1', body: 'test1' }]);
      expect(typeOrmRepository.insert.calledOnce).to.be.true;
    });
  });

  describe('Failure', () => {
    before(() => {
      typeOrmRepository = {
        find: sandbox.stub(),
        findOne: sandbox.stub(),
        insert: sandbox.stub(),
        remove: sandbox.stub(),
      };

      typeOrmRepository.find.rejects();
      typeOrmRepository.remove.rejects();
      typeOrmRepository.insert.rejects();

      // To circumvent sinon overload issue, we create our custom repo mock and cast through unknown to insert
      genericRepository = new GenericRepository<MockDTO, MockEntity>(typeOrmRepository as unknown as Repository<MockEntity>, dataMapper);
    });

    after(() => {
      sandbox.restore();
    });

    it('getAll() method should throw a database error', async () => {
      try {
        await genericRepository.getAll();
      } catch (error) {
        expect(error).to.be.instanceOf(DatabaseError);
      }
    });

    it('remove() method should throw a database error', async () => {
      try {
        await genericRepository.remove([]);
      } catch (error) {
        expect(error).to.be.instanceOf(DatabaseError);
      }
    });

    it('insert() method should throw a database error', async () => {
      try {
        await genericRepository.insert([]);
      } catch (error) {
        expect(error).to.be.instanceOf(DatabaseError);
      }
    });
  });
});
