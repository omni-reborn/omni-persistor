import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import 'mocha';

chai.use(sinonChai);
const expect = chai.expect;

import { MessageDataMapper } from 'src/repository/message';
import { mockMessageEntity } from 'tests/repository/mockMessageEntity';

describe('Message Data Mapper', () => {
  const dataMapper = new MessageDataMapper();

  it('toDomain() method should return a message DTO with keys and values matching message properties', () => {
    const dto = dataMapper.toDomain(mockMessageEntity);
    expect(dto).to.deep.equal(mockMessageEntity);
  });

  it('toEntity() method should return a messageEntity with values from the DTO', () => {
    const dto = {
      id: 'id',
      messageId: 'mId',
      body: 'body',
      channel: 'channel',
      guild: 'guild',
      author: 'author',
      timestamp: new Date(),
    };

    const entity = dataMapper.toEntity(dto);
    expect(entity).to.deep.equal(dto);
  });
});
