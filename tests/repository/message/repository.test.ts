import * as chai from 'chai';
import * as sinon from 'sinon';
import * as sinonChai from 'sinon-chai';
import 'mocha';

chai.use(sinonChai);
const expect = chai.expect;

import { Repository } from 'typeorm';
import { MockRepository } from '../typeorm/mockRepository';
import { DatabaseError } from 'src/error';
import { IMessageRepository, MessageRepository } from 'src/repository/message';
import { MessageEntity } from 'src/entity';
import { mockMessageEntity } from '../mockMessageEntity';

describe('Message Repository', () => {
  const sandbox = sinon.createSandbox();

  const firstMessage = { ...mockMessageEntity, ...{ id: '1' } };
  const secondMessage = { ...mockMessageEntity, ...{ id: '2' } };
  const thirdMessage = { ...mockMessageEntity, ...{ id: '3' } };

  let messageRepository: IMessageRepository;
  let typeOrmRepository: MockRepository<MessageEntity>;

  describe('Success', () => {
    beforeEach(() => {
      typeOrmRepository = {
        find: sandbox.stub(),
      };

      typeOrmRepository.find.withArgs({ author: 'author' }).resolves([firstMessage]);
      typeOrmRepository.find.withArgs({ channel: 'channel' }).resolves([secondMessage]);
      typeOrmRepository.find.withArgs({ guild: 'guild' }).resolves([thirdMessage]);

      // To circumvent sinon overload issue, we create our custom repo mock and cast through unknown to insert
      messageRepository = new MessageRepository(typeOrmRepository as unknown as Repository<MessageEntity>);
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('getAllByAuthor() method should return all rows by given author', async () => {
      const result = await messageRepository.getAllByAuthor('author');
      expect(typeOrmRepository.find.calledOnce).to.be.true;
      expect(result).to.be.an('array');
      expect(result.length).to.be.equal(1);
      expect(result[0]).to.deep.equal(firstMessage);
    });

    it('getAllByChannel() method should return all rows from given channel', async () => {
      const result = await messageRepository.getAllByChannel('channel');
      expect(typeOrmRepository.find.calledOnce).to.be.true;
      expect(result).to.be.an('array');
      expect(result.length).to.be.equal(1);
      expect(result[0]).to.deep.equal(secondMessage);
    });

    it('getAllByGuild() method should return all rows from given guild', async () => {
      const result = await messageRepository.getAllByGuild('guild');
      expect(typeOrmRepository.find.calledOnce).to.be.true;
      expect(result).to.be.an('array');
      expect(result.length).to.be.equal(1);
      expect(result[0]).to.deep.equal(thirdMessage);
    });
  });

  describe('Failure', () => {
    beforeEach(() => {
      typeOrmRepository = {
        find: sandbox.stub(),
      };

      typeOrmRepository.find.rejects();

      // To circumvent sinon overload issue, we create our custom repo mock and cast through unknown to insert
      messageRepository = new MessageRepository(typeOrmRepository as unknown as Repository<MessageEntity>);
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('getAllByAuthor() method should throw a database error', async () => {
      try {
        await messageRepository.getAllByAuthor('author');
      } catch (error) {
        expect(error).to.be.instanceOf(DatabaseError);
      }
    });

    it('getAllByChannel() method should throw a database error', async () => {
      try {
        await messageRepository.getAllByChannel('channel');
      } catch (error) {
        expect(error).to.be.instanceOf(DatabaseError);
      }
    });

    it('getAllByGuild() method should throw a database error', async () => {
      try {
        await messageRepository.getAllByGuild('guild');
      } catch (error) {
        expect(error).to.be.instanceOf(DatabaseError);
      }
    });
  });
});
