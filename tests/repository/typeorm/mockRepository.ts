import * as sinon from 'sinon';
import { FindConditions, FindOneOptions, RemoveOptions, InsertResult } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export interface MockRepository<T> {
  find?: sinon.SinonStub<[FindConditions<T>?], Promise<Array<T>>>;
  findOne?: sinon.SinonStub<[FindConditions<T>, FindOneOptions<T>?], Promise<T>>;
  insert?: sinon.SinonStub<[QueryDeepPartialEntity<T> | QueryDeepPartialEntity<Array<T>>], Promise<InsertResult>>;
  remove?: sinon.SinonStub<[Array<T>, RemoveOptions?], Promise<Array<T>>>;
}
