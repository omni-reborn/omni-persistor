import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import 'mocha';

chai.use(sinonChai);
const expect = chai.expect;

import { reduceBoolean } from 'src/util';

describe('reduceBoolean utility', () => {
  it('Should return true if all elements are true', () => {
    const array = new Array(5).fill(true);
    expect(reduceBoolean(array)).to.be.true;
  });

  it('Should return false if any of the elements are false', () => {
    const array = [true, true, true, false, true];
    expect(reduceBoolean(array)).to.be.false;
  });
});
